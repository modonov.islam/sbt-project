package com.example.operation.impl;

import com.example.operation.OperationStrategy;

import java.util.Stack;

/**
 * Finds the sum of n elements, where n is top element
 */
public class SummationStrategy implements OperationStrategy {

    @Override
    public void calculate(Stack<Double> stack) {
        double sum = 0;
        if (stack.size() > stack.peek()) {
            double topElement = stack.pop();
            for (int i = 0; i < topElement; i++) {
                sum += stack.pop();
            }
            stack.push(sum);
        } else {
            System.out.println("Cannot execute the operation");
        }
    }
}
