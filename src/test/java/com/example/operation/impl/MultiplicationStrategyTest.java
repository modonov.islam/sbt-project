package com.example.operation.impl;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class MultiplicationStrategyTest {

    @Test
    public void calculate() {
        Stack<Double> stack = new Stack<>();
        MultiplicationStrategy multiplicationStrategy = new MultiplicationStrategy();
        stack.push(2.0);
        stack.push(6.0);
        stack.push(3.0);
        multiplicationStrategy.calculate(stack);
        double actual = stack.peek();
        assertNotNull(stack);
        assertEquals(18.0, actual, 0.0001);
    }
}