package com.example.operation.impl;

import com.example.operation.OperationStrategy;

import java.util.Stack;

/**
 * Finds square root of top element in stack, then saves the result instead of it
 */
public class SquareStrategy implements OperationStrategy {

    @Override
    public void calculate(Stack<Double> stack) {
        double result;
        result = Math.sqrt(stack.pop());
        stack.push(Math.ceil(result * 10000) / 10000);
    }
}
