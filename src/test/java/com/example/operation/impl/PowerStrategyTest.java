package com.example.operation.impl;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class PowerStrategyTest {

    @Test
    public void calculate() {
        Stack<Double> stack = new Stack<>();
        PowerStrategy powerStrategy = new PowerStrategy();
        stack.push(2.0);
        stack.push(6.0);
        stack.push(3.0);
        stack.push(2.0);
        powerStrategy.calculate(stack);
        double actual = stack.peek();
        assertNotNull(stack);
        assertEquals(8.0, actual, 0.0001);
    }
}