package com.example.operation.impl;

import com.example.operation.OperationStrategy;

import java.util.Stack;

/**
 * Finds average of elements in stack and saves the result
 */
public class AverageStrategy implements OperationStrategy {

    @Override
    public void calculate(Stack<Double> stack) {
        double result;
        double sum = 0;
        int stackSize = stack.size();
        while (stack.size() > 0) {
            sum += stack.pop();
        }
        result = sum / stackSize;
        stack.push(result);
    }
}
