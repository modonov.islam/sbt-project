package com.example.operation.impl;

import com.example.operation.OperationStrategy;

import java.util.Stack;

/**
 * Finds power of the element in stack, then saves the result instead of it (base number - top element, power - top element-1)
 */
public class PowerStrategy implements OperationStrategy {

    @Override
    public void calculate(Stack<Double> stack) {
        double result;
        if (stack.size() > 1) {
            result = Math.pow(stack.pop(), stack.pop());
            stack.push(result);
        } else {
            System.out.println("Cannot execute the operation");
        }
    }
}
