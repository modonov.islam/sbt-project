package com.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    @Test
    public void compute() {
        Calculator calculator = new Calculator();
        int expectedStackSize = calculator.getStack().size();
        calculator.compute("0");
        assertEquals(expectedStackSize, calculator.getStack().size());

    }
}