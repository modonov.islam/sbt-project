package com.example.operation.impl;

import com.example.operation.OperationStrategy;

import java.util.Stack;

/**
 * Finds multiplication of two elements from stack, then saves the result instead of them
 */
public class MultiplicationStrategy implements OperationStrategy {

    @Override
    public void calculate(Stack<Double> stack) {
        double result;
        if (stack.size() > 1) {
            result = stack.pop() * stack.pop();
            stack.push(result);
        } else {
            System.out.println("Cannot execute the operation");
        }
    }
}
