## Stack basierender Taschenrechner

### Implementierte Operationen

- Operation **+**
- Operation **-**
- Operation *
- Operation **/**
- Operation **^**
- Operation **sqrt**
- Operation **sum**
- Operation **avg**

### Beispiel
```
[]
5
[5.0]
6
[5.0, 6.0]
3
[5.0, 6.0, 3.0]
-
[5.0, 3.0]
4
[5.0, 3.0, 4.0]
+
[5.0, 7.0]
3
[5.0, 7.0, 3.0]
*
[5.0, 21.0]
7
[5.0, 21.0, 7.0]
/
[5.0, 3.0]
4
[5.0, 3.0, 4.0]
avg
[4.0]
4
[4.0, 4.0]
+
[8.0]
sqrt
[2.8285]
```

### Class diagram

![class-diagram](src/main/resources/img/class-diagram.png)