package com.example.operation.impl;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class SquareStrategyTest {

    @Test
    public void calculate() {
        Stack<Double> stack = new Stack<>();
        SquareStrategy squareStrategy = new SquareStrategy();
        stack.push(2.0);
        stack.push(6.0);
        stack.push(3.0);
        stack.push(8.0);
        squareStrategy.calculate(stack);
        double actual = stack.peek();
        assertNotNull(stack);
        assertEquals(2.8285, actual, 0.0001);
    }
}