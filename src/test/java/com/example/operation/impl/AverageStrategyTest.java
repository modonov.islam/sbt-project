package com.example.operation.impl;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class AverageStrategyTest {

    @Test
    public void calculate() {
        Stack<Double> stack = new Stack<>();
        AverageStrategy averageStrategy = new AverageStrategy();
        stack.push(2.0);
        stack.push(3.0);
        stack.push(2.0);
        stack.push(6.0);
        stack.push(3.0);
        stack.push(2.0);
        averageStrategy.calculate(stack);
        double actual = stack.peek();
        assertNotNull(stack);
        assertEquals(3.0, actual, 0.0001);
        assertEquals(1, stack.size());
    }
}