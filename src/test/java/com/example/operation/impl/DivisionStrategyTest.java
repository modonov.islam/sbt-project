package com.example.operation.impl;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class DivisionStrategyTest {

    @Test
    public void calculate() {
        Stack<Double> stack = new Stack<>();
        DivisionStrategy divisionStrategy = new DivisionStrategy();
        stack.push(2.0);
        stack.push(6.0);
        stack.push(3.0);
        divisionStrategy.calculate(stack);
        double actual = stack.peek();
        assertNotNull(stack);
        assertEquals(2.0, actual, 0.0001);
    }
}