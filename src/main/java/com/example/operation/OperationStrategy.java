package com.example.operation;

import java.util.Stack;

public interface OperationStrategy {
    void calculate(Stack<Double> stack);
}
