package com.example;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        String input;
        while (true) {
            calculator.print();
            try {
                input = scanner.nextLine();
                if (input.equals("q")) {
                    break;
                }
                calculator.compute(input);
            } catch (NumberFormatException e) {
                System.out.println("Invalid input");
            }
        }
    }
}
