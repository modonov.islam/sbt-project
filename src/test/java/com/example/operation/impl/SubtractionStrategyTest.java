package com.example.operation.impl;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class SubtractionStrategyTest {

    @Test
    public void calculate() {
        Stack<Double> stack = new Stack<>();
        SubtractionStrategy subtractionStrategy = new SubtractionStrategy();
        stack.push(2.0);
        stack.push(3.5);
        stack.push(5.0);
        subtractionStrategy.calculate(stack);
        double actual = stack.peek();
        assertNotNull(stack);
        assertEquals(1.5, actual, 0.0001);
    }
}