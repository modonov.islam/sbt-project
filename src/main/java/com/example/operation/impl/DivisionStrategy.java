package com.example.operation.impl;

import com.example.operation.OperationStrategy;

import java.util.Stack;

/**
 * Finds quotient of two elements from stack, then saves the result instead of them
 */
public class DivisionStrategy implements OperationStrategy {

    @Override
    public void calculate(Stack<Double> stack) {
        double result;
        if (stack.size() > 1) {
            double arg1 = stack.pop();
            double arg2 = stack.pop();
            result = Math.max(arg1, arg2) / Math.min(arg1, arg2);
            stack.push(Math.ceil(result * 10000) / 10000);
        } else {
            System.out.println("Cannot execute the operation");
        }
    }
}
