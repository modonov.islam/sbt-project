package com.example;

import com.example.operation.OperationStrategy;
import com.example.operation.impl.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Stack based Calculator
 */
public class Calculator {
    private Stack<Double> stack;
    private Map<String, OperationStrategy> map;
    private OperationStrategy strategy;

    public Calculator() {
        stack = new Stack<>();
        map = new HashMap<>();
        initializeOperations();
    }

    /**
     * Initializes all available operations
     */
    private void initializeOperations() {
        map.put("+", new AdditionStrategy());
        map.put("-", new SubtractionStrategy());
        map.put("*", new MultiplicationStrategy());
        map.put("/", new DivisionStrategy());
        map.put("^", new PowerStrategy());
        map.put("sqrt", new SquareStrategy());
        map.put("sum", new SummationStrategy());
        map.put("avg", new AverageStrategy());
    }

    /**
     * Sets necessary operation (Example: if operation is +, then AdditionStrategy)
     * @param strategy type
     */
    public void setStrategy(OperationStrategy strategy) {
        this.strategy = strategy;
    }

    public void executeStrategy(Stack<Double> stack) {
        strategy.calculate(stack);
    }

    /**
     * Makes calculations
     * @param input of user
     */
    public void compute(String input) {
        // checks if input isn't operation symbol and zero
        if (!isOperation(input) && Double.parseDouble(input) != 0) {
            stack.push(Double.parseDouble(input));
        }
        if (!stack.isEmpty()) {
            if (map.containsKey(input)) {
                // sets strategy type depending on operation symbol
                setStrategy(map.get(input));
                executeStrategy(stack);
            }
        }
    }

    /**
     * Prints all elements of stack
     */
    public void print() {
        System.out.println(stack);
    }

    public Stack<Double> getStack() {
        return stack;
    }

    /**
     * Checks if user inputs operation symbol
     * @param operation symbol
     * @return true, if it's operation symbol
     */
    private boolean isOperation(String operation) {
        return operation.equals("+") || operation.equals("-") || operation.equals("*") || operation.equals("/") || operation.equals("^") || operation.equals("sqrt") || operation.equals("sum") || operation.equals("avg");
    }
}
