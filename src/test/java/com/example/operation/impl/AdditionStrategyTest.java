package com.example.operation.impl;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class AdditionStrategyTest {

    @Test
    public void calculate() {
        Stack<Double> stack = new Stack<>();
        AdditionStrategy additionStrategy = new AdditionStrategy();
        stack.push(2.0);
        stack.push(3.5);
        stack.push(2.5);
        additionStrategy.calculate(stack);
        double actual = stack.peek();
        assertNotNull(stack);
        assertEquals(6.0, actual, 0.0001);
    }
}