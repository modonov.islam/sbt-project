package com.example.operation.impl;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class SummationStrategyTest {

    @Test
    public void calculate() {
        Stack<Double> stack = new Stack<>();
        SummationStrategy summationStrategy = new SummationStrategy();
        stack.push(2.0);
        stack.push(3.5);
        stack.push(5.0);
        stack.push(2.0);
        stack.push(3.5);
        stack.push(2.0);
        summationStrategy.calculate(stack);
        double actual = stack.peek();
        assertNotNull(stack);
        assertEquals(5.5, actual, 0.0001);
    }
}